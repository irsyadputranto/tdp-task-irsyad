import './Reset.css';
import './App.css';

function App() {
  return (
    <div className="App">
      <main className='app-main'>

        <section className='page page-1'>
          <header className='header-1'>
            <h1 className='tp1'>Your Best Value Proposition</h1>
            <p className='p1'>“If you don’t try this app, you won’t become the superhero you were meant to be”</p>
          </header>
          <div className='container container-1'>
            <figure className='card card-1'>
              <img src='https://colourlex.com/wp-content/uploads/2021/02/ivory-black-painted-swatch-300x300.jpg.webp' alt='Blank'/>
              <div className='card-caption'>
                <figcaption className='fc1'>Special feature</figcaption>
                <a className='link-1' href='google.com'>see more {'>'}</a>
              </div>
            </figure>
            <figure className='card card-1'>
              <img src='https://colourlex.com/wp-content/uploads/2021/02/ivory-black-painted-swatch-300x300.jpg.webp' alt='Blank'/>
              <div className='card-caption'>
                <figcaption className='fc1'>Special feature</figcaption>
                <a className='link-1' href='google.com'>see more {'>'}</a>
              </div>
            </figure>
            <figure className='card card-1'>
              <img src='https://colourlex.com/wp-content/uploads/2021/02/ivory-black-painted-swatch-300x300.jpg.webp' alt='Blank'/>
              <div className='card-caption'>
                <figcaption className='fc1'>Special feature</figcaption>
                <a className='link-1' href='google.com'>see more {'>'}</a>
              </div>
            </figure>
            <figure className='card card-1'>
              <img src='https://colourlex.com/wp-content/uploads/2021/02/ivory-black-painted-swatch-300x300.jpg.webp' alt='Blank'/>
              <div className='card-caption'>
                <figcaption className='fc1'>Special feature</figcaption>
                <a className='link-1' href='google.com'>see more {'>'}</a>
              </div>
            </figure>
          </div>
        </section>

        <section className='page page-2'>
          <header className='header-2'>
            <h1 className='tp2'>Your Best Value Proposition</h1>
            <p className='p2'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,</p>
          </header>
          <div className='container-2'>
            <figure className='card card-2'>
              <img src='https://colourlex.com/wp-content/uploads/2021/02/ivory-black-painted-swatch-300x300.jpg.webp' alt='Blank'/>
              <div className='card-caption-2'>
                <figcaption className='fc2'>Lorem ipsum dolor sit amet, consectetur adipiscing elit</figcaption>
                <p className='link-2'>see more {'>'}</p>
              </div>
            </figure>
            <figure className='card card-2'>
              <img src='https://colourlex.com/wp-content/uploads/2021/02/ivory-black-painted-swatch-300x300.jpg.webp' alt='Blank'/>
              <div className='card-caption-2'>
                <figcaption className='fc2'>Lorem ipsum dolor sit amet, consectetur adipiscing elit</figcaption>
                <p className='link-2'>see more {'>'}</p>
              </div>
            </figure>
            <figure className='card card-2'>
              <img src='https://colourlex.com/wp-content/uploads/2021/02/ivory-black-painted-swatch-300x300.jpg.webp' alt='Blank'/>
              <div className='card-caption-2'>
                <figcaption className='fc2'>Lorem ipsum dolor sit amet, consectetur adipiscing elit</figcaption>
                <p className='link-2'>see more {'>'}</p>
              </div>
            </figure>
          </div>
        </section>

        <section className='page page-3'>
          <div className='container-3'>
            <figure className='card card-3'>
              <h1 className='p3'>The best way to wireframe a website or landing page</h1>
            </figure>
            <figure className='card card-3'>
              <h3 className='cat-1'>category</h3>
              <h2 className='ban-1'>Keys to writing copy that actually converts and sells users</h2>
            </figure>
            
            <figure className='card card-4'>
              <h3 className='ft-3'>category</h3>
              <h2 className='fc-3'>Keys to writing copy that actually converts and sells users</h2>
              <h2 className='an'>author name</h2>
            </figure>
            <figure className='card card-4'>
              <h3 className='ft-3'>category</h3>
              <h2 className='fc-3'>Keys to writing copy that actually converts and sells users</h2>
              <h2 className='an'>author name</h2>
            </figure>
          </div>
        </section>

        <section className='page page-4'>
          <div className='container-4'>
            <figure className='card-5'>
              <div className='bk-2'>

              </div>
              <div className='bk-1'>
                <p className='an5'>Author name</p>
                <h1 className='p5'>The best way to wireframe a website</h1>
                <h3 className='rm5'>Read More</h3>
              </div>
            </figure>
            <figure className='card-5'>
              <h2 className='ft-5'>Keys to writing copy that actually converts and sells users</h2>
              <h3 className='fc-5'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore...</h3>
              <p className='rm-51'>Read more</p>
            </figure>
            <figure className='card-5'>
              <h2 className='ft-5'>Keys to writing copy that actually converts and sells users</h2>
              <h3 className='fc-5'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore...</h3>
              <p className='rm-51'>Read more</p>
            </figure>
          </div>
        </section>
      </main>
    </div>
  );
}

export default App;
