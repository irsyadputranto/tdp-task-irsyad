import styled from "styled-components";

export const CaptionSpecial = styled.div`
    display: flex;
    flex-direction: column;
    background-color: ${props => props.bgColor || "none"};
    padding: 7.5%;
    justify-content: space-between;
`;

export const CardSpecial = styled.figure`
    display: flex;
    flex-direction: column;
    margin-bottom: 8.5625rem;
    flex: 1 1 0;
`


export const FigCapSpecial = styled.figcaption `
    font-size: 1.25rem
    font-weight: 300;
    margin-bottom: 4.625rem;
    color: black
`

export const LinkSpecial = styled.a`
    font-size: 0.875rem;
    text-decoration: none;
    color: $secondary-font-color;
`


export const CardFeature = styled.figure`
    display: flex;
    flex-direction: column;
    margin: 0 0 3.25rem 0;
    flex: 1 1 0;
`

export const CaptionFeature = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    margin-bottom: 8.25rem;
    padding: 2.125rem 0 3.25rem 0;
`

export const FigCapFeature = styled.figcaption`
    font-size: 1.5rem;
    margin-top: 2.125rem;
    margin-bottom: 0.75rem;
`

export const LinkFeature = styled.a`
    font-size: 0.875rem;
    text-decoration: none;
    color: $secondary-font-color;
`

export const contents = [
    {
      img: 'https://colourlex.com/wp-content/uploads/2021/02/ivory-black-painted-swatch-300x300.jpg.webp',
      title: 'Special Features'
    },
    {
      img: 'https://colourlex.com/wp-content/uploads/2021/02/ivory-black-painted-swatch-300x300.jpg.webp',
      title: 'Special Feature'
    },
    {
      img: 'https://colourlex.com/wp-content/uploads/2021/02/ivory-black-painted-swatch-300x300.jpg.webp',
      title: 'Special Feature'
    },
    {
        img: 'https://colourlex.com/wp-content/uploads/2021/02/ivory-black-painted-swatch-300x300.jpg.webp',
        title: 'Special Feature'
    },
    {
        img: 'https://colourlex.com/wp-content/uploads/2021/02/ivory-black-painted-swatch-300x300.jpg.webp',
        title: 'Special Feature'
    }
  ]
// background-color: ${props => (props.primary ? "#4caf50" : "#008CBA")};
// border: none;
// color: white;
// padding: 15px 32px;
// text-align: center;
// text-decoration: none;
// display: inline-block;
// font-size: 16px;