import React from 'react';
import { JsonDisplay } from './table-data';
import { Table } from "@elevenia/master-ui/components/Atom";



function App() {
    const DisplayData = JsonDisplay[0].products.map(
        (content => {
            return(
                <tr>
                    <td>{content.id}</td>
                    <td>{content.title}</td>
                    <td>{content.price}</td>
                    <td>{content.discountPercentage}</td>
                    <td>{content.rating}</td>
                    <td>{content.stock}</td>
                    <td>{content.category}</td>
                </tr>
            )
        }
    ))
  return(
    <Table>
        <thead>
            <tr>
                <th>Id</th>
                <th>Title</th>
                <th>Price</th>
                <th>Discount</th>
                <th>Rating</th>
                <th>Stock</th>
                <th>Category</th>
            </tr>
        </thead>
        <tbody>
            {DisplayData}
        </tbody>
    </Table>
  ) ;
}

export default App;
