import './Reset.css';
import './App.scss';

// import './AppCopy.css';
import { CardSpecial, CaptionSpecial, FigCapSpecial, LinkSpecial,
// CardFeature, CaptionFeature, FigCapFeature, LinkFeature, 
contents, 
CardFeature,
FigCapFeature,
LinkFeature,
CaptionFeature} from "./components/Card"
import { features } from './dummy-json/JsonDummy';

function App() {

  const cardSpecial = contents.slice(
    contents.length-4, contents.length).map(content => (
    <CardSpecial>
      <img src={content.img} alt='none'/> 
      <CaptionSpecial>
        <FigCapSpecial>{content.title}</FigCapSpecial>
        <LinkSpecial>see more</LinkSpecial>
      </CaptionSpecial>
    </CardSpecial>
  ))

  const cardFeature = features.slice(
    features.length-3, features.length).map(feature => (
    <CardFeature>
      <img src={feature.img} alt='none'/>
      <CaptionFeature>
        <FigCapFeature>{feature.title}</FigCapFeature>
        <LinkFeature>see more</LinkFeature>
      </CaptionFeature>
    </CardFeature>
  ))


  return (
    <div className="App">
      <main className='app-main'>

        {/* <section className="page">
            <header className="header-1">
                <h1 className="tp1">Your Best Value Proposition</h1>
                <p className="p1">“If you don’t try this app, you won’t become the superhero you were meant to be”</p>
            </header>
            <div className="wrapper">
                <figure className='card-1'>
                    <img src='https://colourlex.com/wp-content/uploads/2021/02/ivory-black-painted-swatch-300x300.jpg.webp' alt='Blank'/>
                    <div className='card-caption'>
                        <figcaption className='fc1'>Special feature</figcaption>
                        <a className='link-1' href='google.com'>see more {'>'}</a>
                    </div>
                </figure>
                <figure className='card-1'>
                    <img src='https://colourlex.com/wp-content/uploads/2021/02/ivory-black-painted-swatch-300x300.jpg.webp' alt='Blank'/>
                    <div className='card-caption'>
                        <figcaption className='fc1'>Special feature</figcaption>
                        <a className='link-1' href='google.com'>see more {'>'}</a>
                    </div>
                </figure>
                <figure className='card-1'>
                    <img src='https://colourlex.com/wp-content/uploads/2021/02/ivory-black-painted-swatch-300x300.jpg.webp' alt='Blank'/>
                    <div className='card-caption'>
                        <figcaption className='fc1'>Special feature</figcaption>
                        <a className='link-1' href='google.com'>see more {'>'}</a>
                    </div>
                </figure>
                <figure className='card-1'>
                    <img src='https://colourlex.com/wp-content/uploads/2021/02/ivory-black-painted-swatch-300x300.jpg.webp' alt='Blank'/>
                    <div className='card-caption'>
                        <figcaption className='fc1'>Special feature</figcaption>
                        <a className='link-1' href='google.com'>see more {'>'}</a>
                    </div>
                </figure>
            </div>
        </section> */}

        
        <section>
            <header className="header-1">
                <h1 className="tp1">Your Best Value Proposition</h1>
                <p className="p1">“If you don’t try this app, you won’t become the superhero you were meant to be”</p>
            </header>
            <div className="wrapper">
              {cardSpecial}
            </div>
        </section>        

        <section>
            <header className="header-2">
                <h1 className='tp2'>Your Best Value Proposition</h1>
                <p className='p2'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,</p>
            </header>
            <div className='wrapper'>
                {/* <figure className='card card-2'>
                    <img src='https://colourlex.com/wp-content/uploads/2021/02/ivory-black-painted-swatch-300x300.jpg.webp' alt='Blank'/>
                    <div className='card-caption-2'>
                        <figcaption className='fc2'>Lorem ipsum dolor sit amet, consectetur adipiscing elit</figcaption>
                        <p className='link-2'>see more {'>'}</p>
                    </div>
                    </figure>
                    <figure className='card card-2'>
                    <img src='https://colourlex.com/wp-content/uploads/2021/02/ivory-black-painted-swatch-300x300.jpg.webp' alt='Blank'/>
                    <div className='card-caption-2'>
                        <figcaption className='fc2'>Lorem ipsum dolor sit amet, consectetur adipiscing elit</figcaption>
                        <p className='link-2'>see more {'>'}</p>
                    </div>
                    </figure>
                    <figure className='card card-2'>
                    <img src='https://colourlex.com/wp-content/uploads/2021/02/ivory-black-painted-swatch-300x300.jpg.webp' alt='Blank'/>
                    <div className='card-caption-2'>
                        <figcaption className='fc2'>Lorem ipsum dolor sit amet, consectetur adipiscing elit</figcaption>
                        <p className='link-2'>see more {'>'}</p>
                    </div>
                </figure> */}
                {cardFeature}
            </div>
        </section>

        <section>
          <div className='st1'>
            <figure className='card-3'>
                <h1 className='p3'>The best way to wireframe a website or landing page</h1>
            </figure>
          </div>
          <div className='st2'>
            <figure className='card-3'>
                <h3 className='cat-1'>category</h3>
                <h2 className='ban-1'>Keys to writing copy that actually converts and sells users</h2>
            </figure>
          </div>
          <div className="wrapper-column">
            <figure className='card-4'>
                <h3 className='ft-3'>category</h3>
                <h2 className='fc-3'>Keys to writing copy that actually converts and sells users</h2>
                <h2 className='an'>author name</h2>
            </figure>
            <figure className='card-4'>
                <h3 className='ft-3'>category</h3>
                <h2 className='fc-3'>Keys to writing copy that actually converts and sells users</h2>
                <h2 className='an'>author name</h2>
            </figure>
          </div>
        </section>

        <section>
          <figure className='card-5'>
            <div className='bk-1'>
            </div>
            <div className='bk-2'>
              <p className='an5'>Author name</p>
              <h1 className='p5'>The best way to wireframe a website</h1>
              <h3 className='rm5'>Read More</h3>
            </div>
          </figure>
          <div className='wrapper-row'>
            <figure className='card-6'>
              <h2 className='ft-5'>Keys to writing copy that actually converts and sells users</h2>
              <h3 className='fc-5'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore...</h3>
              <p className='rm-51'>Read more</p>
            </figure>
            <figure className='card-6'>
              <h2 className='ft-5'>Keys to writing copy that actually converts and sells users</h2>
              <h3 className='fc-5'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore...</h3>
              <p className='rm-51'>Read more</p>
            </figure>
          </div>
        </section>
      </main>
    </div>
  );

}
export default App;