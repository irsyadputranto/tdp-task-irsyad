import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
// import App from './todolist/Todolist';
import reportWebVitals from './reportWebVitals';
import { ThemeProvider } from 'styled-components';
import IndomaretTheme from './Theme'
import List from './form/List'


ReactDOM.render(
  <ThemeProvider theme={IndomaretTheme}>
    <List />
  </ThemeProvider>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
