import React, {useState} from 'react'
import "./Form.css"

function Form(props) {

    const [values, setValues] = useState({
        firstName: '',
        lastName: '',
        address: '',
    });
    const [addrtype] = useState(["Data Management", "Product Development & Developer", "Product Design", "HR"])
    const [department, setDepartment] = useState('Data Management')
    const Add = addrtype.map(Add => Add)
    
    const handleAddrTypeChange = (e) => { 
        setDepartment(addrtype[e.target.value]) 
    }

    const handleFirstNameInput = e => {
        setValues((values) => ({
            ...values,
            firstName: e.target.value,
        }));
    }
    const handleLastNameInput = e => {
        setValues((values) => ({
            ...values,
            lastName: e.target.value,
        }));
    }
    
    const handleAddressInput = e => {
        setValues((values) => ({
            ...values,
            address: e.target.value,
        }));
    }
    const handleSubmit = e => {
        e.preventDefault();
        if(values.firstName === ""){
            alert("First name is required")
        } else if(values.lastName === ""){
            alert("Last name is required")
        } else if(values.address === ""){
            alert("Address is required")
        } else {
            props.onSubmit({
                id: Math.floor(Math.random() * 10000),
                firstName: values.firstName,
                lastName: values.lastName,
                role: department,
                address: values.address
            })
        }
    }

  return (
    <form onSubmit={handleSubmit} className='form-container'>
        <div className="input-container">
            <label>First name:</label><br/>
            <input className="input" type="text" id="fname" value={values.firstName} onChange={handleFirstNameInput}/>
        </div>
        <div className="input-container">
            <label>Last name:</label><br/>
            <input className="input" type="text" id="lname" value={values.lastName} onChange={handleLastNameInput}/>
        </div>
        <div className="input-container">
        <label>Department:</label><br/>
            <select className="input" onChange={e => handleAddrTypeChange(e)}>
            {
                Add.map((title, key) => <option key={key}value={key}>{title}</option>)
            }
            </select >
        </div>
        <div className="input-container">
            <label>Address:</label><br/>
            <textarea className="input" type="text" id="department" value={values.address} onChange={handleAddressInput}></textarea><br/>
        </div>

        <button onClick={handleSubmit}>Submit</button>
    </form>
  )
}

export default Form