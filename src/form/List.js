import React, { useState } from 'react'
import TableRow from './DataAtom';
import Form from './Form'
import "./List.scss"


function List() {
    const [datas, setDatas] = useState([]);

    const addData = data => {
        const newDatas = [data, ...datas]

        setDatas(newDatas)
        console.log(...datas)
    }
    const handleDeleteClick = (dataId) => {
        const newDatas = [...datas];
    
        const index = datas.findIndex((data) => data.id === dataId);
    
        newDatas.splice(index, 1);
    
        setDatas(newDatas);
      };
  return (
    <div className='container'>
        <Form className="left" onSubmit={addData}/>
        <table className='right'>
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Address</th>
                    <th>Role</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody className='table-hover'>
                {datas.map((data) => 
                <TableRow key={data.id} id={data.id} data={data} handleDeleteClick={handleDeleteClick}/>)}
            </tbody>
        </table>
    </div>
  )
}

export default List