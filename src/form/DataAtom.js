import React from "react";

const TableRow = ({ data, handleDeleteClick }) => {
  return (
    <tr>
      <td><h3>{data.firstName} {data.lastName}</h3></td>
      <td><h3>{data.address}</h3></td>
      <td><h3>{data.role}</h3></td>
      <td><button onClick={() => handleDeleteClick(data.id)}>Delete</button></td>
    </tr>
  );
};

export default TableRow;