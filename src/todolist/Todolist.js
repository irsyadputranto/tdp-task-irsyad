import DoList from "./List";

function App() {
    return(
    <div className="todo-app">
        <DoList/>
    </div>
    )
}

export default App;