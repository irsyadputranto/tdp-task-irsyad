// import Button from "@elevenia/master-ui/components/Atom/Button/index";
// import Button from "@elevenia/master-ui/components/Atom/Button";
import { CaptionSpecial, CardSpecial, FigCapSpecial, LinkSpecial } from "./components/Card";
import { contents } from "./components/Card";
// import { Button } from "../node_modules/@elevenia/master-ui/components/Atom/Button/index";

function App() {
    const cardSpecial = contents.slice(
        contents.length-4, contents.length).map(content => (
        <CardSpecial>
          <img src={content.img} alt='none'/> 
          <CaptionSpecial bgColor='white'>
            <FigCapSpecial>{content.title}</FigCapSpecial>
            <LinkSpecial>see more</LinkSpecial>
          </CaptionSpecial>
        </CardSpecial>
      ))
    return(
        <div>
            {cardSpecial}
        </div>
        )
}

export default App
